#!/bin/sh
#
# Acquire a lock to protect a "pre" hook against concurrent executions.
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
# SPDX-License-Identifier: WTFPL
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

set -e

LOCKDIR=/run/lock/mylockdir
mkdir "$LOCKDIR" 2>/dev/null || { echo "The enter script had been launched. But the exit script has not." 1>&2; exit 1; }
echo $PPID > "$LOCKDIR/pid"

# Do stuff here
