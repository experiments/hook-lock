#!/bin/sh
#
# Example of how to safely release the lock in the "post" hook.
#
# Copyright (C) 2018  Antonio Ospite <ao2@ao2.it>
# SPDX-License-Identifier: WTFPL
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# http://sam.zoy.org/wtfpl/COPYING for more details.

set -e

LOCKDIR=/run/lock/mylockdir
[ -d "$LOCKDIR" ] || exit 0

LOCKPID=$(cat "$LOCKDIR/pid")
[ "$LOCKPID" = $PPID ] || { echo "The enter script was launched from another process. Unlocking aborted." >&2; exit 1; }

# Use a trap to release the lock even if the commands below fail.
trap 'rm -rf "$LOCKDIR"' EXIT
trap 'exit 2' HUP INT QUIT PIPE TERM

# Do stuff here.
